defmodule DotenvParserTest do
  use ExUnit.Case
  doctest DotenvParser

  setup do
    [
      "BASIC",
      "AFTER_LINE",
      "EMPTY",
      "SINGLE_QUOTES",
      "SINGLE_QUOTES_SPACED",
      "DOUBLE_QUOTES",
      "DOUBLE_QUOTES_SPACED",
      "EXPAND_NEWLINES",
      "EXPAND_MANY_SLASHES",
      "EXPAND_UNICODE",
      "INVALID_UNICODE",
      "DONT_EXPAND_UNQUOTED",
      "DONT_EXPAND_SQUOTED",
      "EQUAL_SIGNS",
      "RETAIN_INNER_QUOTES",
      "RETAIN_LEADING_DQUOTE",
      "RETAIN_LEADING_SQUOTE",
      "RETAIN_TRAILING_DQUOTE",
      "RETAIN_TRAILING_SQUOTE",
      "RETAIN_INNER_QUOTES_AS_STRING",
      "TRIM_SPACE_FROM_UNQUOTED",
      "USERNAME",
      "SPACED_KEY",
      "INLINE_COMMENT",
      "INLINE_COMMENT_PLAIN",
      "END_BACKSLASH",
      "lowercased_var",
      "FOO"
    ]
    |> Enum.each(&System.delete_env/1)
  end

  test "parses correctly" do
    assert DotenvParser.parse_file("test/data/.env") == [
             {"BASIC", "basic"},
             {"AFTER_LINE", "after_line"},
             {"EMPTY", ""},
             {"SINGLE_QUOTES", "single_quotes"},
             {"SINGLE_QUOTES_SPACED", "    single quotes    "},
             {"DOUBLE_QUOTES", "double_quotes"},
             {"DOUBLE_QUOTES_SPACED", "    double quotes    "},
             {"EXPAND_NEWLINES", "expand\nnew\nlines"},
             {"EXPAND_MANY_SLASHES", "expand\\\\nmany \tslashes"},
             {"EXPAND_UNICODE", "expand ሴ"},
             {"INVALID_UNICODE", "not valid \\uarf1"},
             {"DONT_EXPAND_UNQUOTED", "dontexpand\\nnewlines\\u1234"},
             {"DONT_EXPAND_SQUOTED", "dontexpand\\nnewlines"},
             {"EQUAL_SIGNS", "equals=="},
             {"RETAIN_INNER_QUOTES", "{\"foo\": \"bar\"}"},
             {"RETAIN_LEADING_DQUOTE", "\"retained"},
             {"RETAIN_LEADING_SQUOTE", "'retained"},
             {"RETAIN_TRAILING_DQUOTE", "retained\""},
             {"RETAIN_TRAILING_SQUOTE", "retained'"},
             {"RETAIN_INNER_QUOTES_AS_STRING", "{\"foo\": \"bar\"}"},
             {"TRIM_SPACE_FROM_UNQUOTED", "some spaced out string"},
             {"USERNAME", "therealnerdybeast@example.tld"},
             {"SPACED_KEY", "parsed"},
             {"INLINE_COMMENT", "foo#bar"},
             {"INLINE_COMMENT_PLAIN", "foo bar"},
             {"END_BACKSLASH", "something\\"},
             {"lowercased_var", "foo"},
             {"FOO", "exports are supported"}
           ]
  end

  test "loads env" do
    DotenvParser.load_file("test/data/.env")

    assert System.get_env("INLINE_COMMENT") == "foo#bar"
    assert System.get_env("lowercased_var") == "foo"
  end

  test "fails with invalid env and no env is loaded" do
    assert_raise(DotenvParser.ParseError, fn ->
      DotenvParser.load_file("test/data/.invalid_env")
    end)

    assert is_nil(System.get_env("FOO"))
  end

  test "fails with invalid variable names" do
    assert_raise(DotenvParser.ParseError, fn ->
      DotenvParser.parse_file("test/data/.invalid_var_name_start")
    end)

    assert_raise(DotenvParser.ParseError, fn ->
      DotenvParser.parse_file("test/data/.invalid_var_name_chars")
    end)
  end
end
